/**
 * Copyright (C) 2016, 1C
 */
package function;

import elements.TwoPolar;

/**
 * @author ovchi
 *
 */
public class HelpFuncGenerator {

    TwoPolar polar;
    int fi, fj, i;

    public HelpFuncGenerator(TwoPolar func, int fi, int fj, int i) {
        this.polar = func;
        this.fi = fi;
        this.fj = fj;
        this.i = i;
    }

    public TwoPolar getPolar() {
        return polar;
    }

    public Function getFunc() {
        return polar.getFunctions()[i];
    }

    public int getI() {
        return i;
    }

    public int getFi() {
        return fi;
    }

    public int getFj() {
        return fj;
    }

}
