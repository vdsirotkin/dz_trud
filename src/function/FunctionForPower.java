/**
 * Copyright (C) 2016, 1C
 */
package function;

/**
 * @author ovchi
 *
 */
public interface FunctionForPower {
    public double function(double x, double y, double t);
}
