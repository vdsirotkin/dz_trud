/**
 * Copyright (C) 2016, 1C
 */
package function;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import elements.TwoPolar;

/**
 * @author ovchi
 *
 */
public class FunctionGenerator {

    public FunctionGenerator() {
        // TODO Auto-generated constructor stub
    }

    public static Collection<List<HelpFuncGenerator>> generate(List<TwoPolar> elements, int nodes) {
        HashMap<Integer, List<HelpFuncGenerator>> map = new HashMap<>();
        for (int i = 0; i < nodes; i++)
        {
            for (TwoPolar twoPolar : elements)
            {
                if (twoPolar.getFi() == (i + 1))
                {
                    List<HelpFuncGenerator> list = map.get(i + 1);
                    if (list == null)
                    {
                        list = new ArrayList<>();
                    }
                    list.add(new HelpFuncGenerator(twoPolar, twoPolar.getFi(), twoPolar.getFj(), 0));
                    map.put(i + 1, list);
                }
                else if (twoPolar.getFj() == (i + 1))
                {
                    List<HelpFuncGenerator> list = map.get(i + 1);
                    if (list == null)
                    {
                        list = new ArrayList<>();
                    }
                    list.add(new HelpFuncGenerator(twoPolar, twoPolar.getFi(), twoPolar.getFj(), 1));
                    map.put(i + 1, list);
                }
            }
        }
        return map.values();
    }

}
