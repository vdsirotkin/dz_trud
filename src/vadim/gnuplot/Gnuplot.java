package vadim.gnuplot;
import com.panayotis.gnuplot.JavaPlot;
import com.panayotis.gnuplot.plot.DataSetPlot;
import com.panayotis.gnuplot.style.PlotStyle;
import com.panayotis.gnuplot.style.Style;

public class Gnuplot {
	public static void plotWithLines(double[][] a) {
        JavaPlot p = new JavaPlot("C:\\\\Program Files\\gnuplot\\bin\\gnuplot.exe");
//        JavaPlot p = new JavaPlot(System.getProperty("user.dir")+"/gnuplot/bin/gnuplot.exe");
        PlotStyle myPlotStyle = new PlotStyle();
        myPlotStyle.setStyle(Style.LINES);
        DataSetPlot s = new DataSetPlot(a);
        myPlotStyle.setLineWidth(1);
        s.setPlotStyle(myPlotStyle);
        p.setTitle("Lab2");
        p.addPlot(s);
        p.newGraph();
        p.plot();
    }
}