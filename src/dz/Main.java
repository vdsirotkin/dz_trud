/**
 * Copyright (C) 2016,
 */
package dz;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import elements.*;
import function.FunctionGenerator;
import function.HelpFuncGenerator;
import vadim.gnuplot.Gnuplot;

/**
 * @author ovchi
 */
public class Main {

    static double dt = 1e-7;
    static double t = 1e-3;
    static int node = 4;
    static int numE = 2;

    public static void main(String[] args) {
        List<TwoPolar> elements = Arrays.asList(
                new PowerSupply(1, -1, 1),
                new PowerSupplySin(1, 2),
                new Katu(2, 3, 0.001),
                new Rea(1, 3, 1000),
                new Diod(3, 4, 0.073, 1e-12),
                new Conder(4, -1, 1e-6),
                new Rea(4, -1, 1000)
                );
        Collection<List<HelpFuncGenerator>> a = FunctionGenerator.generate(elements, node);
        int numRow = node * 3 + numE;

        List<double[]> dots = new ArrayList<double[]>();
        dots.add(new double[]{0, 0});
        double[] amendment = new double[numRow];
        double[] xArray = new double[numRow];


        int timeNum = (int) (t / dt);
        Matrix obj = new Matrix(numRow, numRow);

        double time = 0.0;
        while (time < t) {
            double[] dupRight;
            do {
                obj.zeroIni();
                for (int i = 0; i < node; i++) {
                    int cell = i + 2 * node;
                    obj.iniCell(i, i, 1);
                    obj.iniCell(i, cell, -1 / dt);
                    obj.iniRight(i, -(amendment[i] - (amendment[cell] - xArray[cell]) / dt));
                }
                for (int i = node; i < 2 * node; i++) {
                    int cell = i + node;
                    obj.iniCell(i, i, 1);
                    obj.iniCell(i, cell, -dt);
                    obj.iniRight(i,
                            -(amendment[i] - xArray[i] - amendment[cell] * dt));
                }
                int forPower = 0;
                for (TwoPolar tp : elements) {
                    int x = tp.getFi() == -1 ? 1 : tp.getFi() - 1;
                    int y = tp.getFj() == -1 ? 1 : tp.getFj() - 1;
                    if (tp instanceof Conder) {
                        int cell = 2 * node;
                        obj.summCell(cell + x, x, tp.getConstMatrix(0, 0, 0, 0, 0));
                        obj.summCell(cell + x, y, tp.getConstMatrix(0, 1, 0, 0, 0));
                        obj.summCell(cell + y, x, tp.getConstMatrix(1, 0, 0, 0, 0));
                        obj.summCell(cell + y, y, tp.getConstMatrix(1, 1, 0, 0, 0));
                    } else if (tp instanceof Katu) {
                        int cell = 2 * node;
                        obj.summCell(cell + x, node + x, tp.getConstMatrix(0, 0, 0, 0, 0));
                        obj.summCell(cell + x, node + y, tp.getConstMatrix(0, 1, 0, 0, 0));
                        obj.summCell(cell + y, node + x, tp.getConstMatrix(1, 0, 0, 0, 0));
                        obj.summCell(cell + y, node + y, tp.getConstMatrix(1, 1, 0, 0, 0));
                    } else if (tp instanceof Rea) {
                        int cell = 2 * node;
                        obj.summCell(cell + x, cell + x, tp.getConstMatrix(0, 0, 0, 0, 0));
                        obj.summCell(cell + x, cell + y, tp.getConstMatrix(0, 1, 0, 0, 0));
                        obj.summCell(cell + y, cell + x, tp.getConstMatrix(1, 0, 0, 0, 0));
                        obj.summCell(cell + y, cell + y, tp.getConstMatrix(1, 1, 0, 0, 0));
                    } else if (tp instanceof PowerSupply || tp instanceof PowerSupplySin) {
                        int cell = 2 * node;
                        int cell2 = 3 * node;
                        obj.summCell(cell2+forPower, cell + x, tp.getConstMatrix(0, 0, 0, 0, 0));
                        obj.summCell(cell2+forPower, cell + y, tp.getConstMatrix(0, 1, 0, 0, 0));
                        obj.summCell(cell + x, cell2+forPower, tp.getConstMatrix(0, 0, 0, 0, 0));
                        obj.summCell(cell + y, cell2+forPower, tp.getConstMatrix(0, 1, 0, 0, 0));
                        obj.iniRight(cell2 + forPower,
                                -tp.getFunctions()[0].function(amendment[x + cell], amendment[y + cell], time));
                        forPower++;
                    } else if (tp instanceof Diod) {
                        int cell = 2 * node;
                        obj.summCell(cell + x, cell + x, tp.getConstMatrix(0, 0, amendment[cell + x], amendment[cell + y], 0));
                        obj.summCell(cell + x, cell + y, tp.getConstMatrix(0, 1, amendment[cell + x], amendment[cell + y], 0));
                        obj.summCell(cell + y, cell + x, tp.getConstMatrix(1, 0, amendment[cell + x], amendment[cell + y], 0));
                        obj.summCell(cell + y, cell + y, tp.getConstMatrix(1, 1, amendment[cell + x], amendment[cell + y], 0));
                    }
                }
                int i = 0;
                for (List<HelpFuncGenerator> list : a) {
                    double summ = 0;
                    for (HelpFuncGenerator polar : list) {
                        int x = polar.getFi() == -1 ? 1 : (polar.getFi() - 1);
                        int y = polar.getFj() == -1 ? 1 : (polar.getFj() - 1);
                        if (polar.getPolar() instanceof PowerSupply || polar.getPolar() instanceof PowerSupplySin) {
                            if (polar.getI() == 0)
                                summ += amendment[3 * node];
                            else
                                summ -= amendment[3 * node];
                        } else if (polar.getPolar() instanceof Katu) {
                            double x0 = amendment[x + node], y0 = amendment[y + node], t0 = 0;
                            summ += polar.getFunc().function(x0, y0, t0);
                        } else if (polar.getPolar() instanceof Rea) {
                            double x0 = amendment[x + 2 * node], y0 = amendment[y + 2 * node], t0 = 0;
                            summ += polar.getFunc().function(x0, y0, t0);
                        } else if (polar.getPolar() instanceof Conder) {
                            double x0 = amendment[x], y0 = amendment[y], t0 = 0;
                            summ += polar.getFunc().function(x0, y0, t0);
                        } else if (polar.getPolar() instanceof Diod) {
                            double x0 = amendment[x + 2 * node], y0 = amendment[y + 2 * node], t0 = 0;
                            summ += polar.getFunc().function(x0, y0, t0);
                        }
                    }
                    obj.summRight(2 * node + i, -summ);
                    i++;

                }
                dupRight = obj.getFullRight();
//                obj.printMatrix();
                obj.gausSlae();
                for (int j = 0; j < numRow; j++) {
                    amendment[j] += obj.getRight(j);
                }
                for (int j = 0; j < numRow; j++) {
                    dupRight[j] = obj.getRight(j) / amendment[j];
                }

            } while (chek(dupRight, 1e-3));
            for (int j = 0; j < numRow; j++) {
                xArray[j] = amendment[j];
            }

            double[] buf = new double[2];
            buf[0] = time;
            buf[1] = xArray[2 * node + 2];
            dots.add(buf);
            time += dt;

        }
        double data[][] = new double[dots.size()][2];

        for (int k = 0; k < dots.size(); k++) {
            data[k][0] = dots.get(k)[0];
            data[k][1] = dots.get(k)[1];
        }

        Gnuplot.plotWithLines(data);
    }

    public static boolean chek(double[] array1, double e) {
        double a = 0.0;
        for (int i = 0; i < array1.length; i++) {
            a += Math.pow(array1[i], 2);
        }
        return Math.sqrt(a) > e;
    }
}
