package dz;
public class Matrix {
	private double[][] cell;
	private double[] right;
	private int a,b;
	Matrix(int a,int b){
		cell=new double[a][b];
		right=new double[a];
		this.a=a;
		this.b=b;
	}

    public double[] getFullRight() {
        return right.clone();
    }
	public void summCell(int i,int j,double a){
		cell[i][j]+=a;
	}
	public void summRight(int i,double a){
		right[i]+=a;
	}
	public void iniCell(int i,int j,double a){
		cell[i][j]=a;
	}
	public void iniRight(int i,double a){
		right[i]=a;
	}
	public double getRight(int i){
		return right[i];
	}

    public void zeroIni() {
        for (int i = 0; i < a; i++)
        {
            for (int j = 0; j < b; j++)
            {
                cell[i][j] = 0.0;
            }
            right[i] = 0.0;
        }
    }
    public void printMatrix() {
		for(int i=0;i<a;i++){
            for (int j = 0; j < b; j++)
            {
                System.out.format("%7.2f\t", cell[i][j]);
            }

            System.out.format("|%e\n", right[i]);
		}
	}

    double exp(double a) {
        return Math.pow(2.718281828, a);
    }
	public void gausSlae(){
		long start=System.nanoTime();
		for(int i=0;i<a;i++){
			double buff=cell[i][i];
			for(int j=i;j<a;j++){
				cell[i][j]/=buff;
			}
			right[i]/=buff;

			for(int j=i+1;j<a;j++){
				buff=cell[j][i];
				for(int k=i;k<a;k++){
					cell[j][k]-=buff*cell[i][k];
				}
				right[j]-=buff*right[i];
			}
		    for(int j=i-1;j>=0;--j){
		    	buff=cell[j][i];
				for(int k=i;k<a;k++){
				    cell[j][k]-=cell[i][k]*buff;
				}
				right[j]-=right[i]*buff;
		    }
		}
		long end = System.nanoTime();
		long traceTime = end-start;
		System.out.println("СЛАУ была решена за t="+traceTime/Math.pow(10, 9)+" (sec)\n");
	}
	public double[][] getCell(){
		return cell;
	}


}
