/**
 * Copyright (C) 2016, 1C
 */
package elements;

import function.Function;

/**
 * @author ovchi
 *
 */
public class Rea
    extends TwoPolar {

    /**
     * @param fi
     * @param fj
     */
    public Rea(int fi, int fj, double constant) {
        super(fi, fj);
        constMatrix = new Function[2][2];
        functions = new Function[2];
        if (fi == -1 || fj == -1)
        {
            if (fi == -1)
            {
                functions[1] = (x, y, t) -> (-1) / constant * (-y);
            }
            else if (fj == -1)
            {
                functions[0] = (x, y, t) -> (1) / constant * (x);
            }
            constMatrix[fi == -1 ? 1 : 0][fi == -1 ? 1 : 0] =(x, y, t) -> 1 / constant;
            constMatrix[fi == -1 ? 0 : 1][fi == -1 ? 0 : 1] =(x, y, t) -> 0.0;
            constMatrix[0][1] =(x, y, t) -> 0.0;
            constMatrix[1][0] =(x, y, t) -> 0.0;
        }
        else
        {
            functions[0] = (x, y, t) -> 1 / constant * (x - y);
            functions[1] = (x, y, t) -> -1 / constant * (x - y);
            constMatrix[0][0] =(x, y, t) -> 1 / constant;
            constMatrix[0][1] =(x, y, t) -> -1 / constant;
            constMatrix[1][0] =(x, y, t) -> -1 / constant;
            constMatrix[1][1] =(x, y, t) -> 1 / constant;
        }
    }

}
