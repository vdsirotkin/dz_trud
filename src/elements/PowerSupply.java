/**
 * Copyright (C) 2016, 1C
 */
package elements;

import function.Function;

/**
 * @author ovchi
 *
 */
public class PowerSupply
    extends TwoPolar {

    /**
     * @param fi
     * @param fj
     */
    public PowerSupply(int fi, int fj, double constant) {
        super(fi, fj);
        constMatrix = new Function[2][2];
        functions = new Function[2];
        if (fj == -1 || fi == -1)
        {
            functions[0] = (x, y, t) -> x - constant;
            constMatrix[fi == -1 ? 1 : 0][fi == -1 ? 1 : 0] =(x, y, t) -> 1;
            constMatrix[fi == -1 ? 0 : 1][fi == -1 ? 0 : 1] =(x, y, t) -> 0.0;
            constMatrix[0][1] =(x, y, t) -> 0.0;
            constMatrix[1][0] =(x, y, t) -> 0.0;
        }
        else
        {
            functions[0] = (x, y, t) -> (x - y - constant);
            constMatrix[0][0] = (x, y, t) ->1;
            constMatrix[0][1] = (x, y, t) ->-1;
            constMatrix[1][0] =(x, y, t) -> -1;
            constMatrix[1][1] =(x, y, t) -> 1;
        }
    }

}
