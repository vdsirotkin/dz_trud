package elements;

import function.Function;

/**
 * Created by ovchi on 13.12.2016.
 */
public class Diod extends TwoPolar {

    public Diod(int fi, int fj,double mft,double Ie) {
        super(fi, fj);
        constMatrix=new Function[2][2];
        functions=new Function[2];

        if (fi == -1 || fj == -1)
        {
            if (fi == -1)
            {
                functions[1] = (x, y, t) -> -Ie * (Math.exp(( - y)/mft)-1);
            }
            else if (fj == -1)
            {
                functions[0] = (x, y, t) -> Ie * (Math.exp((x )/mft)-1);
            }
            constMatrix[fi == -1 ? 1 : 0][fi == -1 ? 1 : 0] =(x, y, t) -> Ie/mft * (Math.exp((x - y)/mft));
            constMatrix[fi == -1 ? 0 : 1][fi == -1 ? 0 : 1] =(x, y, t) -> 0.0;
            constMatrix[0][1] = (x, y, t) -> 0.0;
            constMatrix[1][0] = (x, y, t) -> 0.0;


        }
        else
        {
            functions[0] = (x, y, t) -> Ie * (Math.exp((x - y)/mft)-1);
            functions[1] = (x, y, t) -> -Ie * (Math.exp((x - y)/mft)-1);
            constMatrix[0][0] = (x, y, t) -> Ie/mft * (Math.exp((x - y)/mft));
            constMatrix[0][1] = (x, y, t) -> -Ie/mft * (Math.exp((x - y)/mft));
            constMatrix[1][0] = (x, y, t) -> -Ie/mft * (Math.exp((x - y)/mft));
            constMatrix[1][1] = (x, y, t) -> Ie/mft * (Math.exp((x - y)/mft));
        }
    }
}
