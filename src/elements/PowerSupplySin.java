/**
 * Copyright (C) 2016, 1C
 */
package elements;

import function.Function;

/**
 * @author ovchi
 *
 */
public class PowerSupplySin
    extends TwoPolar {

    double A = 10;
    double T = 1e-4;

    public PowerSupplySin(int fi, int fj) {
        super(fi, fj);
        constMatrix = new Function[2][2];
        functions = new Function[2];
        if (fj == -1)
        {
            functions[0] = (x, y, t) -> -x + A * Math.sin(t / T * 4 * Math.asin(1));
            constMatrix[fi == -1 ? 1 : 0][fi == -1 ? 1 : 0] =(x, y, t) -> 1;
            constMatrix[fi == -1 ? 0 : 1][fi == -1 ? 0 : 1] =(x, y, t) -> 0.0;
            constMatrix[0][1] =(x, y, t) -> 0.0;
            constMatrix[1][0] =(x, y, t) -> 0.0;

        }
        else
        {
            functions[0] = (x, y, t) ->+(x - y)-A * Math.sin(t / T * 4 * Math.asin(1))  ;
            constMatrix[0][0] = (x, y, t) ->1;
            constMatrix[0][1] =(x, y, t) -> -1;
            constMatrix[1][0] =(x, y, t) -> -1;
            constMatrix[1][1] =(x, y, t) -> 1;
        }
    }


}
