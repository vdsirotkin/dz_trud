/**
 * Copyright (C) 2016, 1C
 */
package elements;

import function.Function;

/**
 * @author ovchi
 *
 */
public class TwoPolar{
    Function[][] constMatrix;
    Function[] functions;
    int fi,fj;
    public TwoPolar( int fi, int fj) {
        this.fi = fi;
        this.fj = fj;
    }

    public double getConstMatrix(int i, int j,double x,double y,double t) {

        return constMatrix[i][j].function(x,y,t);

    }

    public Function[] getFunctions() {
        return functions;
    }

    public int getFi() {
        return fi;
    }

    public int getFj() {
        return fj;
    }

}
